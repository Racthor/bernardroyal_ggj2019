﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class BalancingToolData : ScriptableObject
{
    public float cameraMoveTime { get; set; }
    public float cameraMoveDistance { get; set; }
    public float cameraMoveWaitTime { get; set; }

    public float waveMoveWaitTime { get; set; }

    public float beberSpeed { get; set; }
    public float[] beberSpeedVariation { get; set; }
    public float timerBeforeFreeze { get; set; }

    public GameObject[] spawnPoints { get; set; }
    public Object[] coquilles_prefab { get; set; }
    public PalierData[] regleSpawnCoquille { get; set; }

    public AudioClip[] sonBernardAh;
    public AudioClip[] sonBernardCoquin;
    public AudioClip[] sonBernardOhNon;
    public AudioClip[] sonBernardYouhou;
    public AudioClip sonBernardRentreDansMaison;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
