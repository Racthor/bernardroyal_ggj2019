﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour 
{
	public void startGame() {
			Debug.Log("Start game");
			SceneManager.LoadScene ("Player_Selection");
	}
	
	public void quitGame() {
			Debug.Log("Quit game");
			Application.Quit();
	}
    
}