﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private float timer;
    private Texture2D[] chiffresCompteur;
    private float timerFreeze;
    [HideInInspector] public bool isFrozen = false;

    public float clock;
    private float startTimeFreeze;

    [HideInInspector] public int nbEvent;
    public RawImage compteurDisplay;
    public Text finMessage;
    public Text finInfoMessage;

    public AudioClip[] sonsMouettes;
    public AudioSource sonMouette;

    [HideInInspector] public Camera mainCamera;
    [HideInInspector] public GameObject vagueDestructriceDeMolusques;
    [HideInInspector] public GameObject[] lesBernards;
    [HideInInspector] public GameObject[] lesBernardsStockage;
    private GameObject[] paliers;
    private GameObject predator;

    private Animator animVague;

    private GameInitializer gameInitializer;
    private EndGame endGame;

    private void InitializeOnAwake()
    {
        gameInitializer = this.GetComponent<GameInitializer>();
        endGame = this.GetComponent<EndGame>();

        paliers = GameObject.FindGameObjectsWithTag("Palier");

        chiffresCompteur = gameInitializer.InitializeCompteur();
        predator = gameInitializer.InitializePredator();
    }

    private void InitializeOnStart()
    {

    }

    void Awake()
    {
        InitializeOnAwake();
    }



    // Start is called before the first frame update
    void Start()
    {
        //Initialisation
        mainCamera = gameInitializer.InitializeCamera(mainCamera);
        vagueDestructriceDeMolusques = gameInitializer.InitializeVague(vagueDestructriceDeMolusques);

        //Player initialization
        lesBernardsStockage = gameInitializer.InitializeBernards();
        lesBernards = new GameObject[lesBernardsStockage.Length];
        for (int i = 0; i < lesBernardsStockage.Length; ++i)
        {
            lesBernards[i] = (GameObject)Instantiate(lesBernardsStockage[i]);
            lesBernards[i].SetActive(true);
        }
        lesBernards = gameInitializer.InitializeBernardValues(lesBernards);

        startTimeFreeze = gameInitializer.InitializerTimer();
        gameInitializer.InitializeCoquilles(paliers);

        timer = 0;
        nbEvent = 0;
        isFrozen = false;
        if (vagueDestructriceDeMolusques == null)
        {
            Debug.LogError("Y a pas assez de vagues, c'est null ! Recommencez !");
        }
        animVague = vagueDestructriceDeMolusques.GetComponent<Animator>();
        sonMouette = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!endGame.goodEnd && !endGame.badEnd)
        {
            if (timer >= startTimeFreeze)
            {
                if (isFrozen)
                {
                    //Attente de la fin des évènements
                    if (animVague.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.90 && animVague.GetCurrentAnimatorStateInfo(0).IsName("Rising_tide"))
                    {
                        isFrozen = false;

                        timer = 0;
                        if (nbEvent++ < 6)
                        {
                            foreach (GameObject beber in lesBernards)
                            {
                                beber.GetComponent<PlayerController>().enabled = true;
                            }

                            foreach (GameObject beber in lesBernards)
                            {
                                beber.GetComponent<PlayerController>().sizeUp();
                            }

                            if (nbEvent < 5) mainCamera.GetComponent<CameraController>().launchMove();
                            vagueDestructriceDeMolusques.GetComponent<VagueMovement>().launchMove();
                        }
                    }
                }
                else
                {
                    //Keep commands separate even if code is a little redundant

                    //Commandes retirée aux joueurs
                    foreach (GameObject beber in lesBernards)
                    {
                        beber.GetComponent<PlayerController>().enabled = false;
                        beber.GetComponent<Animator>().SetBool("IsWalking", false);
                    }

                    //Joue un son de mouette
                    sonMouette.clip = sonsMouettes[new System.Random().Next(0, 7)];
                    sonMouette.Play();

                    //Pour chaque Joueur
                    foreach (GameObject beber in lesBernards)
                    {
                        //Event des mouettes
                        if (beber.activeSelf && beber.GetComponent<PlayerController>().coquille == null)
                        {
                            GameObject clone = Instantiate(predator);
                            clone.GetComponentInChildren<PredatorController>().beberCible = beber;
                            clone.transform.position = beber.GetComponent<PlayerController>().spawnPredator.transform.position;

                        }
                    }
                   
                    isFrozen = true;
                }
            }
            else
            {
                timer += Time.deltaTime /* clock*/;
                if ((int)timer < 5)
                    compteurDisplay.texture = chiffresCompteur[4-(int)timer];
            }
        }
        else
        {
            finMessage.text = endGame.endMessage;
            finInfoMessage.text = endGame.endInfoMessage;
        }

        endGame.LesBernards = lesBernards;
        endGame.lesBernardsStockage = lesBernardsStockage;
        endGame.nbEvent = nbEvent;

        //Test for end conditions before using the result
        endGame.TestForEnd();
    }


}
