﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    
    private AudioSource endGameMusic;
    private bool firstPassage;

    [HideInInspector] public bool goodEnd;
    [HideInInspector] public bool badEnd;
    [HideInInspector] public string endMessage;
    [HideInInspector] public string endInfoMessage;

    public AudioClip audioLost;
    public AudioClip audioWin;

    [HideInInspector] public int nbEvent;
    [HideInInspector] public GameObject[] LesBernards { get; set; }
    [HideInInspector] public GameObject[] lesBernardsStockage { get; set; }

    // Start is called before the first frame update
    void Start()
    {

        //Debug.LogWarning("Remettre la condition de victoire à un joueur !");

        badEnd = false;
        goodEnd = false;
        endMessage = "";
        endInfoMessage = "";
        firstPassage = true;
        endGameMusic = gameObject.GetComponent<AudioSource>();

        if (audioLost == null || audioWin == null)
            Debug.Log("Toutes les pistes audio de fin de jeu ne sont pas assignées.");
    }



    void End(int enVie, int tailleCoquille, int egalite, int bernardWin) {
        //nbEvent == 5 ne peut être atteint que si au moins deux bernards sont arrivés jusqu'au bout
        if (nbEvent == 5)
        {
            if (tailleCoquille == egalite)
            {
                endMessage = "Egalité ! Bravo aux participants.";
            } else
            {
                endMessage = "Bravo au joueur " + bernardWin.ToString() + " !";
            }
            goodEnd = true;
        } else
        {
            if (enVie == 0) {
                endMessage = "Vous avez perdu !";
                badEnd = true;
            }
            if (enVie == 1)
            {
                endMessage = "Bravo au joueur " + bernardWin.ToString() + " !";;
                goodEnd = true;
            }

        }

        if (badEnd || goodEnd)
        {
            foreach (GameObject beber in LesBernards)
            {
                beber.GetComponent<PlayerController>().enabled = false;
            }
            endInfoMessage = "A, Enter : new game. B, G : back to main menu.";
            
            if (firstPassage)
            {
                if (badEnd)
                {
                    endGameMusic.clip = audioLost;
                    endGameMusic.Play();
                }
                else
                {
                    foreach (GameObject beber in LesBernards)
                    {
                        if (beber.activeSelf == true)
                            beber.GetComponent<PlayerController>().PlaySoundYouhou();
                    }
                    endGameMusic.clip = audioWin;
                    endGameMusic.Play();
                }
                firstPassage = false;
            }

            //gestion touche
            if (Input.GetButtonDown("Cancel"))
            {
                foreach (GameObject beber in lesBernardsStockage)
                    DestroyImmediate(beber);
                foreach (GameObject beber in LesBernards)
                    DestroyImmediate(beber);

                SceneManager.LoadScene("Title_Screen");
            }
                
            if (Input.GetButtonDown("Submit") || Input.GetKeyDown(KeyCode.Return))
            {
                foreach (GameObject beber in lesBernardsStockage)
                {
                    beber.SetActive(true);
                    DontDestroyOnLoad(beber);
                }
                foreach (GameObject beber in LesBernards)
                    DestroyImmediate(beber);

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
                


        }

    }


    public bool TestForEnd()
    {

        //test mort / taille
        int enVie = 0;
        int tailleCoquille = 1;
        int egalite = 1;
        int compteur = 1;
        int bernardWin = 1;
        foreach (GameObject bernard in LesBernards)
        {
            if (bernard.activeInHierarchy)
            {
                enVie++;
                if (bernard.GetComponent<PlayerController>().coquille != null)
                {
                    int tailleCetteCoquille = bernard.GetComponent<PlayerController>().coquille.GetComponent<CoquilleController>().size;

                    if (tailleCoquille < tailleCetteCoquille)
                    {
                        tailleCoquille = tailleCetteCoquille;
                        bernardWin = compteur;
                    }
                    else
                    {
                        if (tailleCoquille == tailleCetteCoquille)
                            egalite = tailleCetteCoquille;

                    }

                    compteur++;
                }

            }
        }

        //Debug.Log("enVie : " + enVie);

        End(enVie, tailleCoquille, egalite, bernardWin);

        return goodEnd || badEnd;
    }
}
