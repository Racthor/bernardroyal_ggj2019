﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalancingTool : MonoBehaviour
{
    public bool updateDataWhilePlaying = false;

    public BalancingToolData balanceData;

    public float cameraMoveTime = 5f;
    public float cameraMoveDistance = 100f;
    public float cameraMoveWaitTime = 0f;

    public float waveMoveWaitTime = 0f;

    public float beberSpeed = 100f;
    public float[] beberSpeedVariation = { 1.0f, 0.9f, 0.85f, 0.75f, 0.7f, 0.65f, 0.6f, 0.9f };
    public float timerBeforeFreeze = 5f;

    public GameObject[] spawnPoints;
    private Object[] coquilles_prefab;
    public PalierData[] regleSpawnCoquille;

    private AudioClip[] sonBernardAh;
    private AudioClip[] sonBernardCoquin;
    private AudioClip[] sonBernardOhNon;
    private AudioClip[] sonBernardYouhou;
    private AudioClip sonBernardRentreDansMaison;


    private void UpdateBalancingData()
    {
        coquilles_prefab = GetCoquille();
        sonBernardAh = GetSonBernardAh();
        sonBernardCoquin = GetSonBernardCoquin();
        sonBernardOhNon = GetSonBernardOhNon();
        sonBernardYouhou = GetSonBernardYouhou();
        sonBernardRentreDansMaison = GetSonBernardRentreMaison();

        if (coquilles_prefab.Length == 0)
            Debug.LogError("Seashell sweet seashell...Except there's none.");
        if (balanceData == null)
            Debug.LogError("Missing balancing data !");
        if (cameraMoveTime == 0)
            Debug.LogWarning("Camera moving time is 0, the camera will instantly jump to its new position.");
        if (beberSpeed == 0)
            Debug.LogError("Bernards are stuck to the ground, their speed is 0 !");
        if (timerBeforeFreeze == 0)
            Debug.LogError("No time before freezeingand launching event, player won't be able to play !");

        balanceData.cameraMoveTime = cameraMoveTime;
        balanceData.cameraMoveDistance = cameraMoveDistance;
        balanceData.cameraMoveWaitTime = cameraMoveWaitTime;
        balanceData.waveMoveWaitTime = waveMoveWaitTime;
        balanceData.beberSpeed = beberSpeed;
        balanceData.beberSpeedVariation = beberSpeedVariation;
        balanceData.timerBeforeFreeze = timerBeforeFreeze;
        balanceData.spawnPoints = spawnPoints;
        balanceData.regleSpawnCoquille = regleSpawnCoquille;
        balanceData.coquilles_prefab = coquilles_prefab;

        balanceData.sonBernardAh = sonBernardAh;
        balanceData.sonBernardCoquin = sonBernardCoquin;
        balanceData.sonBernardOhNon = sonBernardOhNon;
        balanceData.sonBernardYouhou = sonBernardYouhou;
        balanceData.sonBernardRentreDansMaison = sonBernardRentreDansMaison;
    }

    private Object[] GetCoquille()
    {
        return Resources.LoadAll("Prefab/Coquille", typeof(GameObject));
    }

    private AudioClip[] GetSonBernardAh()
    {
        return Resources.LoadAll<AudioClip>("Sounds/BernardlHermite/Ah");
    }

    private AudioClip[] GetSonBernardCoquin()
    {
        return Resources.LoadAll<AudioClip>("Sounds/BernardlHermite/Coquin");
    }

    private AudioClip[] GetSonBernardOhNon()
    {
        return Resources.LoadAll<AudioClip>("Sounds/BernardlHermite/OhNon");
    }

    private AudioClip[] GetSonBernardYouhou()
    {
        return Resources.LoadAll<AudioClip>("Sounds/BernardlHermite/Youhouhou");
    }

    private AudioClip GetSonBernardRentreMaison()
    {
        return Resources.Load<AudioClip>("Sounds/BernardlHermite/rentreDansMaison");
    }

    // Start is called before the first frame update
    void Awake()
    {
        UpdateBalancingData();
    }

    // Update is called once per frame
    void Update()
    {
        if (updateDataWhilePlaying)
        {
            UpdateBalancingData();
            //updateDataWhilePlaying = false;
        }
    }
}
