﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class GameInitializer : MonoBehaviour
{
    public BalancingToolData balanceData;


    public GameObject[] InitializeBernardValues(GameObject[] lesBernards)
    {
        foreach (GameObject beber in lesBernards)
        {
            beber.GetComponent<PlayerController>().speed = balanceData.beberSpeed;
            beber.GetComponent<PlayerController>().speedVariation = balanceData.beberSpeedVariation;
            beber.GetComponent<PlayerController>().sonBernardAh = balanceData.sonBernardAh;
            beber.GetComponent<PlayerController>().sonBernardCoquin = balanceData.sonBernardCoquin;
            beber.GetComponent<PlayerController>().sonBernardOhNon = balanceData.sonBernardOhNon;
            beber.GetComponent<PlayerController>().sonBernardYouhou = balanceData.sonBernardYouhou;
            beber.GetComponent<PlayerController>().rentreMaison = balanceData.sonBernardRentreDansMaison;
        }
        return lesBernards;
    }

    // Start is called before the first frame update
    public GameObject[] InitializeBernards()
    {
        GameObject[] lesBernards = GameObject.FindGameObjectsWithTag("Player");

        if (lesBernards.Length == 0)
        {
            Debug.LogError("Error : No Players present in this scene");
        } else
        {
            int i = 0;
            foreach (GameObject beber in lesBernards)
            {
                beber.GetComponent<PlayerController>().speed = balanceData.beberSpeed;
                beber.GetComponent<PlayerController>().speedVariation = balanceData.beberSpeedVariation;
                beber.GetComponent<PlayerController>().sonBernardAh = balanceData.sonBernardAh;
                beber.GetComponent<PlayerController>().sonBernardCoquin = balanceData.sonBernardCoquin;
                beber.GetComponent<PlayerController>().sonBernardOhNon = balanceData.sonBernardOhNon;
                beber.GetComponent<PlayerController>().sonBernardYouhou = balanceData.sonBernardYouhou;
                beber.GetComponent<PlayerController>().rentreMaison = balanceData.sonBernardRentreDansMaison;

                if (beber.transform.GetChild(1).childCount > 0)
                    DestroyImmediate(beber.transform.GetChild(1).GetChild(0).gameObject);

                beber.transform.position = balanceData.spawnPoints[i++].transform.position;
                beber.SetActive(false);
            }
        }

        return lesBernards;
    }
    
    public Camera InitializeCamera(Camera mainCamera)
    {
        mainCamera.GetComponent<CameraController>().time = balanceData.cameraMoveTime;
        mainCamera.GetComponent<CameraController>().distance = balanceData.cameraMoveDistance;
        mainCamera.GetComponent<CameraController>().waitTime = balanceData.cameraMoveWaitTime;
        return mainCamera;
    }

    public GameObject InitializeVague(GameObject vague)
    {
        vague.GetComponent<VagueMovement>().time = balanceData.cameraMoveTime;
        vague.GetComponent<VagueMovement>().distance = balanceData.cameraMoveDistance;
        vague.GetComponent<VagueMovement>().waitTime = balanceData.waveMoveWaitTime;
        return vague;
    }

    public float InitializerTimer()
    {
        return balanceData.timerBeforeFreeze;
    }

    public void InitializeCoquilles(GameObject[] paliers)
    {
        List<GameObject> spawnpointCoquilles = new List<GameObject>(0);
        Object[] prefab_coquilles;
        GameObjectSearcher searcher = new GameObjectSearcher();

        for(int i = 0; i < paliers.Length - 1; i++)
        {
            spawnpointCoquilles.Clear();
            prefab_coquilles = balanceData.coquilles_prefab;

            GameObject palier = paliers[i];
            //Get all spawnPoints from that palier
            spawnpointCoquilles = searcher.FindObjectwithTag("SpawnCoquille", palier.transform);
            List<int> coquilleToSpawn = new List<int>(0);

            for (int j = 0; j < balanceData.regleSpawnCoquille[i].coquilleToSpawn.Length; j++)
            {
                if (balanceData.regleSpawnCoquille[i].coquilleToSpawn[j] != 0)
                {
                    for (int k = 0; k < balanceData.regleSpawnCoquille[i].coquilleToSpawn[j]; k++)
                    {
                        //Store the list of seashells that need to be spawned for that specific palier
                        coquilleToSpawn.Add(j);
                    }
                }  
            }
                
            //Spawn all seashells needed
            while (coquilleToSpawn.Count > 0 && spawnpointCoquilles.Count > 0)
            {
                //Select a random and free spawnPoint for the seashell to spawn at
                int pos = Random.Range(0, spawnpointCoquilles.Count);
                //Instantiate a seashell at the selected location
                Instantiate(balanceData.coquilles_prefab[coquilleToSpawn[Random.Range(0,coquilleToSpawn.Count)]], spawnpointCoquilles[pos].transform);
                //Remove the spawnpoint from the list because it now hosts a seashell
                spawnpointCoquilles.Remove(spawnpointCoquilles[pos]);
                //Remove the seashell from the list of seashells waiting to be spawned
                coquilleToSpawn.Remove(coquilleToSpawn[0]);
            }
        }
    }

    
    public Texture2D[] InitializeCompteur()
    {
        return Resources.LoadAll<Texture2D>("Textures/UI");
    }

    public GameObject InitializePredator()
    {
        return Resources.Load<GameObject>("Prefab/Predator");

    }
}
