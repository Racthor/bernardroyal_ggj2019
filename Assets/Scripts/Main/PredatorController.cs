﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorController : MonoBehaviour
{

    private Animator anim;
    private AudioSource audioSource;
    public GameObject beberCible { get; set; }

    private static IEnumerator FadeOut(AudioSource audio, float FadeTime)
    {
        float startVolume = audio.volume;
        while (audio.volume > 0)
        {
            audio.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
        }
        audio.Stop();
    }

    void Awake()
    {
        anim = this.GetComponentInParent<Animator>();
        audioSource = this.GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && other.gameObject == beberCible)
        {
            audioSource.clip = other.gameObject.GetComponent<PlayerController>().GetSoundManger();
            audioSource.Play();
            StartCoroutine(FadeOut(audioSource, 1f));
            other.gameObject.SetActive(false);
        }

    }

    void Animating()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 && anim.GetCurrentAnimatorStateInfo(0).IsName("banzai"))
                DestroyImmediate(this.gameObject.transform.parent.gameObject);
    }

    void Update()
    {
        Animating();
    }
}
