﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoVelocity : MonoBehaviour
{
    private Rigidbody thisRigidbody;

    // Start is called before the first frame update
    void Awake()
    {
        thisRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (thisRigidbody != null)
        {
            thisRigidbody.velocity = new Vector3(0, 0, 0);
            thisRigidbody.angularVelocity = new Vector3(0, 0, 0);
        }
    }
}
