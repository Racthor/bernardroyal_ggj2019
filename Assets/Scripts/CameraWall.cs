﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWall : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Coquille"))
        {
            Physics.IgnoreCollision(this.gameObject.GetComponent<Collider>(), collision.collider, true);
        }
    }
}
