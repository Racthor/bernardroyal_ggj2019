﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoquilleController : MonoBehaviour
{
    public int size = 1;
    public int value = 1;
    private Animator anim;
    private bool alive = true;
    private AudioSource seBrise;
    private bool notPlayed;

    // Start is called before the first frame update
    void Awake()
    {
        seBrise = gameObject.GetComponent<AudioSource>();
        anim = gameObject.GetComponent<Animator>();
        anim.SetBool("destruction", alive);
        notPlayed = true;
    }

    public int GetSize()
    {
        return size;
    }

    public int GetValue()
    {
        return value;
    }

    public void SetAlive(bool a)
    {
        alive = a;
    }

    void Animating()
    {
        anim.SetBool("destruction", alive);
        if (!alive)
        {
            if (!seBrise.isPlaying && notPlayed)
            {
                seBrise.Play();
                notPlayed = false;
            }
            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 && anim.GetCurrentAnimatorStateInfo(0).IsName("Destruction"))
            {
                DestroyImmediate(this.gameObject);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Animating();
    }
}
