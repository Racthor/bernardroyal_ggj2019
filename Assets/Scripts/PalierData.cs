﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PalierData
{
    [NamedArray (new string[] { "Coquille 1", "Coquille 2", "Coquille 3", "Coquille 4", "Coquille 5", "Coquille 6", "Coquille 7" })]
    public int[] coquilleToSpawn = new int[7];
}