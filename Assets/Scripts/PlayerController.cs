﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    /*public string axeX = "Horizontal1";
    public string axeY = "Vertical1";
    public string action = "Action1";*/

    public float speed { get; set; }
    public float[] speedVariation { get; set; }

    public int isJoystick = -1;
    public string axeX = "Horizontal1_clavier";
    public string axeY = "Vertical1_clavier";
    public string action = "Action1_keyboard";
    public string cancel = "Cancel1_keyboard";
    public Player_manager p_manager = null;
    public int player_num = 0;
    
    private Vector3 movement;
    private Animator anim;
    private Rigidbody playerRigidbody;
    public int size = 1;
    [HideInInspector] public GameObject coquille = null;
    private GameObject coqtmp = null;

    private MeshRenderer rend;
    private GameObject beber;
    private AudioSource audioSource;
    [HideInInspector] public Transform spawnPredator { get; set; }

    [HideInInspector] public AudioClip[] sonBernardAh { get; set; }
    [HideInInspector] public AudioClip[] sonBernardCoquin { get; set; }
    [HideInInspector] public AudioClip[] sonBernardOhNon { get; set; }
    [HideInInspector] public AudioClip[] sonBernardYouhou { get; set; }
    [HideInInspector] public AudioClip rentreMaison { get; set; }



    private static IEnumerator FadeOut(AudioSource audio, float FadeTime)
    {
        float startVolume = audio.volume;
        while (audio.volume > 0)
        {
            audio.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
        }
        audio.Stop();
    }

    private void Awake()
    {
        float[] defaultSpeedVariation = { 1.0f, 0.9f, 0.85f, 0.75f, 0.7f, 0.65f, 0.6f, 0.9f };

        speed = 50f;
        speedVariation = defaultSpeedVariation;
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        spawnPredator = transform.GetChild(2);
    }

    private void Start()
    {
        beber = this.transform.GetChild(0).gameObject;
        
        rend = beber.GetComponent<MeshRenderer>();
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    //*************Sounds******************
    public AudioClip GetSoundManger()
    {
        if (new System.Random().Next(0,5) == 4)
        {
            return sonBernardCoquin[new System.Random().Next(0, sonBernardCoquin.Length)];
        }
        return sonBernardAh[new System.Random().Next(0, sonBernardAh.Length)];
    }

    public void PlaySoundOhNon()
    {
        audioSource.clip = sonBernardOhNon[new System.Random().Next(0, sonBernardOhNon.Length)];
        audioSource.Play();
        StartCoroutine(FadeOut(audioSource, 1.5f));
    }

    public void PlaySoundYouhou()
    {
        audioSource.clip = sonBernardYouhou[new System.Random().Next(0, sonBernardYouhou.Length)];
        audioSource.Play();
    }

    public void PlaySoundRentreMaison()
    {
        audioSource.clip = rentreMaison;
        audioSource.Play();
    }

    //*************Event******************

    public void sizeUp()
    {
        //Debug.Log("beber : " + beber.transform.localScale);
        if (size < 6)
        {
            size += 1;

            if (coquille != null)
            {
                coquille.transform.position = PositionnementCoquille(coquille);
                if (size > coquille.GetComponent<CoquilleController>().GetSize())
                {
                    coquille.GetComponent<CoquilleController>().SetAlive(false);
                    coquille = null;
                    coqtmp = null;
                }
            }

            beber = this.transform.GetChild(0).gameObject;
            beber.transform.localScale += new Vector3(0.15f, 0.15f, 0);
        }
    }

    //*************Movement******************

    private float Speed()
    {
        if (coquille != null)
            return speed * speedVariation[coquille.GetComponent<CoquilleController>().GetSize() - this.size];
        return speed * speedVariation[speedVariation.Length - 1];
    }

    private void Move(float h, float v)
    {
        movement = new Vector3(h, 0, v);
        movement = movement.normalized * this.Speed() * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    //*************Seashell******************

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Coquille"))
        {
            if (size <= other.gameObject.GetComponent<CoquilleController>().size)
            {
                coqtmp = other.gameObject;
            }
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Coquille") && other.gameObject != coquille)
        {
            if (size <= other.gameObject.GetComponent<CoquilleController>().size)
            {
                coqtmp = other.gameObject;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        coqtmp = null;
    }

    private Vector3 PositionnementCoquille(GameObject seashell)
    {
        if (seashell.GetComponent<CoquilleController>().size < 3)
        {
            return new Vector3(this.transform.GetChild(1).position.x,
            this.transform.GetChild(1).position.y,
            this.transform.GetChild(1).position.z + (this.transform.GetChild(0).transform.localScale.x - 10 * seashell.transform.localScale.x));
        }
        return new Vector3(this.transform.GetChild(1).position.x,
            this.transform.GetChild(1).position.y + (this.transform.GetChild(0).transform.localScale.x - seashell.transform.localScale.x),
            this.transform.GetChild(1).position.z);
    }

    void GestionCoquille()
    {
        if (Input.GetButtonDown(action))
        {
            if (coquille != null)
            {
                if (coqtmp != null)
                {
                    //Debug.Log("echange coquille");
                    //Reactivate collisions beetween the seashell and the player
                    Physics.IgnoreCollision(this.gameObject.GetComponent<Collider>(), coquille.GetComponent<Collider>(), false);
                    coquille.transform.SetParent(null);
                    coquille.AddComponent<Rigidbody>();
                    coquille.GetComponent<Rigidbody>().freezeRotation = true;
                    coquille.GetComponent<Rigidbody>().useGravity = false;
                    coquille.AddComponent<NoVelocity>();

                    GameObject tmp = coquille;
                    tmp.transform.position = coqtmp.transform.position;
                    coqtmp.transform.position = PositionnementCoquille(coquille);
                    coquille = coqtmp;
                    coqtmp = null;

                    coquille.transform.SetParent(this.transform.GetChild(1));
                    coquille.transform.position = this.transform.GetChild(1).position;
                    Destroy(coquille.GetComponent<Rigidbody>());
                    Destroy(coquille.GetComponent<NoVelocity>());

                    PlaySoundRentreMaison();
                    //Ignore collisions beetween the worn seashell and the player
                    Physics.IgnoreCollision(this.gameObject.GetComponent<Collider>(), coquille.GetComponent<Collider>(), true);
                }
                else
                {
                    //Debug.Log("coquille lachee");
                    //Reactivate collisions beetween the seashell and the player
                    coquille.transform.SetParent(null);
                    coquille.AddComponent<Rigidbody>();
                    coquille.GetComponent<Rigidbody>().freezeRotation = true;
                    coquille.GetComponent<Rigidbody>().useGravity = false;
                    coquille.AddComponent<NoVelocity>();

                    Physics.IgnoreCollision(this.gameObject.GetComponent<Collider>(), coquille.GetComponent<Collider>(), false);
                    coquille = null;
                }
            }
            else
            {
                if (coqtmp != null)
                {
                    //Debug.Log("coquille ramassee");
                    coquille = coqtmp;
                    coqtmp = null;

                    coquille.transform.SetParent(this.transform.GetChild(1));
                    coquille.transform.position = PositionnementCoquille(coquille);
                    Destroy(coquille.GetComponent<Rigidbody>());
                    Destroy(coquille.GetComponent<NoVelocity>());

                    PlaySoundRentreMaison();
                    //Ignore collisions beetween the worn seashell and the player
                    Physics.IgnoreCollision(this.gameObject.GetComponent<Collider>(), coquille.GetComponent<Collider>(), true);
                }
            }
        }
    }

    //*************Player management******************

    void DeletePlayer()
    {
        // si on est dans le menu de sélection, et qu'on appuie qur la touche cancel, cela supprime le joueur
        if (Input.GetButtonDown(cancel))
        {
            p_manager.current_player--;
            p_manager.isSpawnEmpty[player_num] = true;
            if (isJoystick > -1)
            {
                p_manager.isJoystickAssigned[isJoystick] = false;
            }
            p_manager.players_array[player_num] = null;
            Destroy(this.gameObject);
        }
    }

    private void Animating(float h, float v)
    {
        anim.SetBool("IsWalking", h != 0f || v != 0f);
        anim.SetInteger("Size", size);
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxisRaw(axeX);
        float v = Input.GetAxisRaw(axeY);

        if (SceneManager.GetActiveScene().name == "Player_Selection")
            DeletePlayer();

        Move(h, v);
        GestionCoquille();
        Animating(h, v);
    }
}