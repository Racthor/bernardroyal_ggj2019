﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_manager : MonoBehaviour
{
    private int max_player;
    [HideInInspector] public int current_player;

    string[] keyboard_input = { "[1]", "[2]", "[3]", "[4]" };
    string[] controller_input = { "Action1_joystick", "Action2_joystick", "Action3_joystick", "Action4_joystick" };
    string[] keyboard_X_control = { "Horizontal1_clavier", "Horizontal2_clavier", "Horizontal3_clavier", "Horizontal4_clavier" };
    string[] keyboard_Y_control = { "Vertical1_clavier", "Vertical2_clavier", "Vertical3_clavier", "Vertical4_clavier" };
    string[] joystick_X_control = { "Horizontal1_joystick", "Horizontal2_joystick", "Horizontal3_joystick", "Horizontal4_joystick" };
    string[] joystick_Y_control = { "Vertical1_joystick", "Vertical2_joystick", "Vertical3_joystick", "Vertical4_joystick" };
    string[] keyboard_action_control = { "Action1_keyboard", "Action2_keyboard", "Action3_keyboard", "Action4_keyboard" };
    string[] joystick_action_control = { "Action1_joystick", "Action2_joystick", "Action3_joystick", "Action4_joystick" };
    string[] keyboard_cancel_control = { "Cancel1_keyboard", "Cancel2_keyboard", "Cancel3_keyboard", "Cancel4_keyboard" };
    string[] joystick_cancel_control = { "Cancel1_joystick", "Cancel2_joystick", "Cancel3_joystick", "Cancel4_joystick" };

    public GameObject[] spawn_point;
    public bool[] isSpawnEmpty = { true, true, true, true };
    public bool[] isJoystickAssigned = { false, false, false, false };

    private Object[] player_prefab;
    [HideInInspector] public GameObject[] players_array;


    void Awake()
    {
        max_player = 4;
		current_player = 0;
        players_array = new GameObject[4];
        player_prefab = Resources.LoadAll("Prefab/BernardlHermite", typeof(GameObject));
    }

    // démarrer les jeu si il y a au moins 1 joueur et qu'on appuie sur G
    //Passe les beber d'une scene à l'autre
    void SceneSwap()
    {
        if (Input.GetKeyDown(KeyCode.G) && current_player >= 2)
        {
            //Preserve players through scene loading
            foreach (GameObject player in players_array)
            {
                if (player != null)
                {
                    DontDestroyOnLoad(player);
                }
            }

            SceneManager.LoadScene("Main");
        }
    }


    private GameObject InstantiateKeyboard(int here)
    {
        GameObject player = Instantiate((GameObject)player_prefab[here]);
        player.transform.position = new Vector3(spawn_point[here].transform.position.x, -55, spawn_point[here].transform.position.z);

        player.GetComponent<PlayerController>().player_num = here;
        player.GetComponent<PlayerController>().axeX = keyboard_X_control[here];
        player.GetComponent<PlayerController>().axeY = keyboard_Y_control[here];
        player.GetComponent<PlayerController>().action = keyboard_action_control[here];
        player.GetComponent<PlayerController>().cancel = keyboard_cancel_control[here];
        player.GetComponent<PlayerController>().isJoystick = -1;
        player.GetComponent<PlayerController>().p_manager = this.gameObject.GetComponent<Player_manager>();
        current_player++;

        return player;
    }


    private GameObject InstantiateJoystick(int here, int joystickAssigned)
    {
        GameObject player = Instantiate((GameObject)player_prefab[here]);
        player.transform.position = new Vector3(spawn_point[here].transform.position.x, -55, spawn_point[here].transform.position.z);

        player.GetComponent<PlayerController>().player_num = here;
        player.GetComponent<PlayerController>().axeX = joystick_X_control[joystickAssigned];
        player.GetComponent<PlayerController>().axeY = joystick_Y_control[joystickAssigned];
        player.GetComponent<PlayerController>().action = joystick_action_control[joystickAssigned];
        player.GetComponent<PlayerController>().cancel = joystick_cancel_control[joystickAssigned];
        player.GetComponent<PlayerController>().isJoystick = joystickAssigned;
        player.GetComponent<PlayerController>().p_manager = this.gameObject.GetComponent<Player_manager>();
        current_player++;

        return player;
    }


    private int FindJoystick()
    {
        if (Input.GetButtonDown("Action1_joystick"))
        {
            Debug.Log("joystick 1 pressed");
            return 0;
        }
        if (Input.GetButtonDown("Action2_joystick"))
        {
            Debug.Log("joystick 2 pressed");
            return 1;
        }
        if (Input.GetButtonDown("Action3_joystick"))
        {
            Debug.Log("joystick 3 pressed");
            return 2;
        }
        if (Input.GetButtonDown("Action4_joystick"))
        {
            Debug.Log("joystick 4 pressed");
            return 3;
        }

        return -1;
    }


    private int FindFreeSpawn()
    {
        int i = 0;
        do {
            if (isSpawnEmpty[i])
                return i;
        } while (i++ < max_player) ;
        return -1;
    }


    void Update()
    {
        SceneSwap();
        
        // si appuie sur Entrer et joueur max pas atteint, spawn d'un joueur
        if (Input.GetKeyDown(KeyCode.Return) && current_player < max_player) {

			int here = FindFreeSpawn();
            isSpawnEmpty[here] = false;
           
            players_array[here] = InstantiateKeyboard(here);
			
		}

        // si max pas atteint && button submit est pressé, spawn d'un joueur manette
        if (Input.GetButtonDown("Submit") && current_player < max_player) {;

            int joystickAssigned = FindJoystick();

			if (!isJoystickAssigned[joystickAssigned]) {
				isJoystickAssigned[joystickAssigned] = true;

                int here = FindFreeSpawn();
                isSpawnEmpty[here] = false;
                
                players_array[here] = InstantiateJoystick(here, joystickAssigned);

            } else {
				Debug.Log("All joysticks assigned");
			}
		}
    }
}
