﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectSearcher
{
    public List<GameObject> FindObjectwithTag(string _tag, Transform parent)
    {
        List<GameObject> actors = new List<GameObject>(0);
        return GetChildObject(parent, _tag);
    }

    public List<GameObject> GetChildObject(Transform parent, string _tag)
    {
        List<GameObject> actors = new List<GameObject>(0);
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (child.tag == _tag)
            {
                actors.Add(child.gameObject);
            }
            if (child.childCount > 0)
            {
                actors.AddRange(GetChildObject(child, _tag));
            }
        }
        return actors;
    }
}