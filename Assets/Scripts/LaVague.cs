﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaVague : MonoBehaviour
{

    private Stack<GameObject> toDestroy;
    private Animator anim;

    public GameObject terrain;
    private GameController gameController;
    private AudioSource deferlante;

    // Start is called before the first frame update
    void Start()
    {
        anim = gameObject.GetComponentInParent<Animator>();
        gameController = terrain.GetComponent<GameController>();
        toDestroy = new Stack<GameObject>();
        toDestroy.Clear();
        deferlante = gameObject.GetComponent<AudioSource>();
    }

    void Animating(bool isFrozen)
    {
        anim.SetBool("IsFrozen", isFrozen);
        if (!deferlante.isPlaying && isFrozen)
        {
            deferlante.Play();
        } else if (!isFrozen)
        {
            deferlante.Stop();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameController.isFrozen && toDestroy.Count > 0)
        {
            foreach(GameObject go in toDestroy)
            {
                go.transform.position = new Vector3 (this.transform.position.x+400, go.transform.position.y, go.transform.position.z);
            }
        }
        //Debug.Log(toDestroy.Count);
        //Debug.Log(gameController.isFrozen);
        if (!gameController.isFrozen)
        {
            while (toDestroy.Count > 0)
            {
                toDestroy.Pop().SetActive(false);
            }
            toDestroy.Clear();
        }
        Animating(gameController.isFrozen);
        
    }

    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if (other.CompareTag("Coquille") || other.CompareTag("Player"))
        {
            if (other.CompareTag("Coquille") && other.transform.parent != null && other.transform.parent.CompareTag("emplacement_coquille"))
                return;
            toDestroy.Push(other.gameObject);
            if (other.CompareTag("Player"))
            {
                //Plays a random "oh non" sound when washed away
                other.gameObject.GetComponent<PlayerController>().PlaySoundOhNon();
            }
            //Debug.Log(other.gameObject.name);
        }
            
    }

}
