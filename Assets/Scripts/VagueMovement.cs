﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VagueMovement : MonoBehaviour
{
    public float distance { get; set; }
    public float time { get; set; }
    public float waitTime { get; set; }
    private Rigidbody cameraRigidbody;
    private Vector3 movement;
    private Vector3 target;
    private bool canMove = false;

    // Start is called before the first frame update
    void Awake()
    {
        //if (distance == 0) Debug.LogError("No distance set");
        //if (time == 0) Debug.LogError("No time set");
        cameraRigidbody = gameObject.GetComponent<Rigidbody>();
    }

    IEnumerator waiter()
    {
        yield return new WaitForSeconds(waitTime);
    }

    public void launchMove()
    {
        StartCoroutine(waiter());
        movement = new Vector3(-distance, 0, 0);
        target = new Vector3(transform.position.x + distance, transform.position.y, transform.position.z);
        canMove = true;
    }

    public void Move()
    {
        if (transform.position.x >= target.x)
            canMove = false;
        if (canMove)
            transform.Translate(movement.normalized * (Time.deltaTime * (distance / time)));
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
    }
}
