﻿using UnityEngine;

#if UNITY_EDITOR
using System;
using UnityEditor;
#endif


#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(NamedArrayAttribute))]
public class CoquilleArrayDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        try
        {
            string[] splitPath = property.propertyPath.Split('[', ']');
            int pos = int.Parse(splitPath[3]);
            
            EditorGUI.PropertyField(rect, property, new GUIContent(((NamedArrayAttribute)attribute).names[pos]));
        }
        catch
        {
            EditorGUI.PropertyField(rect, property, label);
        }
    }
}
#endif